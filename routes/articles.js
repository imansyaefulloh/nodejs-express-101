const express = require('express');
const router = express.Router();

const Article = require('../models/article');
const User = require('../models/user');

router.get('/add', ensureAuthenticated, (req, res) => {
  res.render('add_article', {
    title: 'Add Articles'
  });
});

router.get('/:id', (req, res) => {
  Article.findById(req.params.id, (err, article) => {
    User.findById(article.author, (err, user) => {
      res.render('article', {
        article,
        author: user.name
      });
    });
  });
});

router.post('/add', ensureAuthenticated, (req, res) => {
  req.checkBody('title', 'Title is required').notEmpty();
  // req.checkBody('author', 'Author is required').notEmpty();
  req.checkBody('body', 'Body is required').notEmpty();

  let errors = req.validationErrors();

  if (errors) {
    res.render('add_article', {
      title: 'Add Article',
      errors
    });
  } else {
    let { title, body } = req.body;

    let article = new Article();
    article.title = title;
    article.author = req.user._id;
    article.body = body;

    article.save((err) => {
      if(err) {
        console.log(err);
        return;
      } else {
        req.flash('success', 'Article added');
        res.redirect('/');
      }
    });
  }
});

router.get('/edit/:id', ensureAuthenticated, (req, res) => {
  Article.findById(req.params.id, (err, article) => {
    if (article.author != req.user._id) {
      req.flash('danger', 'Not Authorized');
      res.redirect('/');
    } else {
      res.render('edit_article', {
        title: 'Edit Article',
        article
      });
    }
  });
});

router.post('/edit/:id', ensureAuthenticated, (req, res) => {
  let { title, body } = req.body;

  let article = {};
  article.title = title;
  article.body = body;

  let query = {_id: req.params.id};

  Article.update(query, article, (err) => {
    if(err) {
      console.log(err);
      return;
    } else {
      req.flash('success', 'Article updated');
      res.redirect('/');
    }
  });
});

router.delete('/:id', ensureAuthenticated, (req, res) => {
  if (!req.user._id) {
    res.status(500).send();
  }

  let query = {_id: req.params.id};

  Article.findById(req.params.id, (err, article) => {
    if (article.author != req.user._id) {
      res.status(500).send();
    } else {
      Article.remove(query, function(err) {
        if (err) {
          console.log(err);
        }
        res.send('Success');
      });
    }
  });
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('danger', 'Please login');
    res.redirect('/users/login');
  }
};

module.exports = router;